/*
 * Tema 1 - SO2 - Kprobe based tracer
 * Nume: Codrut-Cristian Grosu
 * Grupa: 341C3
 * email: codrut.cristian.grosu@gmail.com
 */

#ifndef TRACER_H__
#define TRACER_H__ 1

#include <asm/ioctl.h>
#ifndef __KERNEL__
#include <sys/types.h>
#endif /* __KERNEL__ */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>
#include <linux/sched.h>
#include <linux/wait.h>
#include <linux/miscdevice.h>
#include <linux/list.h>

#include <linux/proc_fs.h>
#include <linux/seq_file.h>

#include <linux/kprobes.h>
#include<linux/slab.h>

#define TRACER_DEV_MINOR 42
#define TRACER_DEV_NAME "tracer"
#define TRACER_MAXACTIVE 40
#define MAX_COUNT 1024
#define ERROR_ALLOC -1
#define BUFFER_SIZE 256

#define TRACER_ADD_PROCESS	_IOW(_IOC_WRITE, 42, pid_t)
#define TRACER_REMOVE_PROCESS	_IOW(_IOC_WRITE, 43, pid_t)

struct tracer_proc {
	/* the pid of the monitored process */
	pid_t pid;

	/* the number of calls for the kmalloc() function */
	int kmalloc_calls;

	/* the amount of memory allocated for each kmalloc() call */
	unsigned long alloc[MAX_COUNT];

	/* the address returned by each kmalloc() call */
	unsigned long address[MAX_COUNT];

	/* rwlock associated with the kmalloc() call */
	rwlock_t lock_kmalloc;

	/* the number of calls for the kfree() function */
	int kfree_calls;

	/* the amount of memory freed by each kfree() call*/
	unsigned long free[MAX_COUNT];

	/* rwlock associated with the kfree() call */
	rwlock_t lock_kfree;

	/* the number of calls for the schedule() function */
	int schedule_calls;

	/* rwlock associated with the variable schedule_calls */
	rwlock_t lock_schedule_calls;

	/* the number of calls for the up() function */
	int up_calls;

	/* rwlock associated with the variable up_calls */
	rwlock_t lock_up_calls;

	/* the number of calls for the dow_interruptible() function */
	int down_interruptible_calls;

	/* rwlock associated with the variable down_interruptible_calls */
	rwlock_t lock_down_calls;

	/* the number of calls for the mutex_lock() function */
	int mutex_lock_calls;

	/* rwlock associated with the variable mutex_lock_calls */
	rwlock_t lock_lock_calls;

	/* the number of calls for the mutex_unlock() function */
	int mutex_unlock_calls;

	/* rwlock associated with the variable mutex_unlock_calls */
	rwlock_t lock_unlock_calls;

	struct list_head list;
};

struct private_data {
	unsigned long size;
};

static int tracer_proc_show(struct seq_file *, void *);
static int kmalloc_entry_handler(struct kretprobe_instance *, struct pt_regs *);
static int kmalloc_ret_handler(struct kretprobe_instance *, struct pt_regs *);
static int tracer_proc_read_open(struct inode *, struct file *);
static int tracer_open(struct inode *, struct file *);
static int tracer_close(struct inode *, struct file *);
static long tracer_ioctl(struct file *, unsigned int, unsigned long);

static struct tracer_proc *alloc_tracer_proc_elem(pid_t);
static int add_elem(pid_t);
static void remove_elem(pid_t);

static void print_value(struct seq_file *, int, const char *);
static void purge_list(void);
bool cmp_addr(char objp_buff[BUFFER_SIZE], unsigned long addr);
static unsigned long sum_array(unsigned long [], int);

static void tracer_kfree(const void *);
static void __sched tracer_mutex_lock(struct mutex *, unsigned int);
static void __sched tracer_mutex_unlock(struct mutex *);
asmlinkage __visible void __sched tracer_schedule(void);
static void tracer_up(struct semaphore *);
static int tracer_down(struct semaphore *);

#endif /* TRACER_H_ */
