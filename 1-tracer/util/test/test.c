#include<stdio.h>
#include<stdlib.h>

#include<fcntl.h>
#include<string.h>
#include<asm/ioctl.h>

#define TRACER_ADD_PROCESS	_IOW(_IOC_WRITE, 42, pid_t)
#define TRACER_REMOVE_PROCESS	_IOW(_IOC_WRITE, 43, pid_t)

int main (int argc, char** argv)
{

	int fd, pid, error;

	if (argc !=3) {
		printf("./test a|r pid\n");
		return -1;
	}
	fd = open("/dev/tracer", O_RDWR);
	if (fd < 0) {
		printf("Error open\n");
		return -1;
	}

	error = 0;
	pid = atoi(argv[2]);
	switch(argv[1][0]) {
		case 'a':
			if (ioctl(fd, TRACER_ADD_PROCESS, pid) < 0)
			{
				printf("Error - add ioctl\n");
				error = -1;
			}
			break;
		case 'r':
			if (ioctl(fd, TRACER_REMOVE_PROCESS, pid) < 0)
			{
				printf("Error - remove ioctl\n");
				error = -1;
			}
			break;
	}



	close(fd);

	return error;
}
