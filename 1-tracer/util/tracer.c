/*
 * Tema 1 - SO2 - Kprobe based tracer
 * Nume: Codrut-Cristian Grosu
 * Grupa: 341C3
 * email: codrut.cristian.grosu@gmail.com
 */

#ifndef _TRACER_C
#define _TRACER_C

#include "tracer.h"

MODULE_DESCRIPTION("Tema 1 - SO2 - Tracer");
MODULE_AUTHOR("Codrut Grosu, 341C3");
MODULE_LICENSE("GPL");

#define LOG_LEVEL	KERN_ALERT

#define procfs_file_read	"tracer"

static struct proc_dir_entry *proc_tracer_read;

static struct list_head tracer_list;

static spinlock_t lock_list;

static struct kretprobe kmalloc_kretprobe = {
	.handler		= kmalloc_ret_handler,
	.entry_handler		= kmalloc_entry_handler,
	.data_size		= sizeof(struct private_data),
	.maxactive		= TRACER_MAXACTIVE,
};

static const struct file_operations tracer_fops = {
	.owner			= THIS_MODULE,
	.open			= tracer_open,
	.release		= tracer_close,
	.unlocked_ioctl		= tracer_ioctl,
};

static const struct file_operations procfs_fops = {
	.owner			= THIS_MODULE,
	.open			= tracer_proc_read_open,
	.read			= seq_read,
	.release		= single_release,
};

static struct miscdevice tracer_device = {
	.minor			= TRACER_DEV_MINOR,
	.name			= TRACER_DEV_NAME,
	.fops			= &tracer_fops,
};

static struct jprobe kfree_jprobe = {
	.entry			= (kprobe_opcode_t *) tracer_kfree,
};

static struct jprobe mutex_lock_jprobe = {
	.entry			= (kprobe_opcode_t *) tracer_mutex_lock,
};

static struct jprobe mutex_unlock_jprobe = {
	.entry			= (kprobe_opcode_t *) tracer_mutex_unlock,
};

static struct jprobe schedule_jprobe = {
	.entry			= (kprobe_opcode_t *) tracer_schedule,
};

static struct jprobe up_jprobe = {
	.entry			= (kprobe_opcode_t *) tracer_up,
};

static struct jprobe down_jprobe = {
	.entry			= (kprobe_opcode_t *) tracer_down,
};

static struct tracer_proc *alloc_tracer_proc_elem(pid_t pid)
{
	struct tracer_proc *data = (struct tracer_proc *)
		kmalloc(sizeof(struct tracer_proc), GFP_ATOMIC);

	if (data == NULL) {
		pr_err("kmalloc failed\n");
		return NULL;
	}

	data->pid = pid;
	data->kmalloc_calls = 0;
	data->kfree_calls = 0;
	data->schedule_calls = 0;
	data->up_calls = 0;
	data->down_interruptible_calls = 0;
	data->mutex_lock_calls = 0;
	data->mutex_unlock_calls = 0;

	// initialise rwlock for mutex_unlock_calls
	rwlock_init(&data->lock_unlock_calls);

	// initialise rwlock for mutex_lock_calls
	rwlock_init(&data->lock_lock_calls);

	// initialise rwlock for down_interruptible_calls
	rwlock_init(&data->lock_down_calls);

	// initialise rwlock for up_calls
	rwlock_init(&data->lock_up_calls);

	// initialise rwlock for schedule_calls
	rwlock_init(&data->lock_schedule_calls);

	// initialise rwlock for kfree() call
	rwlock_init(&data->lock_kfree);

	// initialise rwlock for kmalloc() call
	rwlock_init(&data->lock_kmalloc);

	return data;
}

static int tracer_down(struct semaphore *sem)
{
	struct list_head *it1, *it2;
	struct tracer_proc *elem;
	int pid = current->pid;
	unsigned long flag1, flag2;

	// acquire the spinlock for the list
	spin_lock_irqsave(&lock_list, flag2);

	list_for_each_safe(it1, it2, &tracer_list) {
		elem = list_entry(it1, struct tracer_proc, list);

		// update the number for down_interruptible() calls
		if (elem->pid == pid) {

			// acquire the rwlock to modify the variable
			write_lock_irqsave(&elem->lock_down_calls, flag1);
			elem->down_interruptible_calls += 1;
			write_unlock_irqrestore(&elem->lock_down_calls, flag1);

			break;
		}
	}

	// release the spinlock for the list
	spin_unlock_irqrestore(&lock_list, flag2);

	jprobe_return();

	return 0;
}

static void tracer_up(struct semaphore *sem)
{
	struct list_head *it1, *it2;
	struct tracer_proc *elem;
	int pid = current->pid;
	unsigned long flag1, flag2;

	// acquire the spinlock for the list
	spin_lock_irqsave(&lock_list, flag2);

	list_for_each_safe(it1, it2, &tracer_list) {
		elem = list_entry(it1, struct tracer_proc, list);

		// update the number of up() calls
		if (elem->pid == pid) {

			// acquire the rwlock to modify the variable
			write_lock_irqsave(&elem->lock_up_calls, flag1);
			elem->up_calls += 1;
			write_unlock_irqrestore(&elem->lock_up_calls, flag1);

			break;
		}
	}

	// release the spinlock for the list
	spin_unlock_irqrestore(&lock_list, flag2);

	jprobe_return();
}

asmlinkage __visible void __sched tracer_schedule(void)
{
	struct list_head *it1, *it2;
	struct tracer_proc *elem;
	int pid = current->pid;
	unsigned long flag1, flag2;

	// acquire the spinlock for the list
	spin_lock_irqsave(&lock_list, flag2);

	list_for_each_safe(it1, it2, &tracer_list) {
		elem = list_entry(it1, struct tracer_proc, list);

		// update the number of schedule() calls
		if (elem->pid == pid) {

			// acquire the rwlock to modify the variable
			write_lock_irqsave(&elem->lock_schedule_calls, flag1);
			elem->schedule_calls += 1;
			write_unlock_irqrestore(&elem->lock_schedule_calls,
						flag1);

			break;
		}
	}

	// release the pinlock for the list
	spin_unlock_irqrestore(&lock_list, flag2);

	jprobe_return();
}

static void __sched tracer_mutex_unlock(struct mutex *lock)
{
	struct list_head *it1, *it2;
	struct tracer_proc *elem;
	int pid = current->pid;
	unsigned long flag1, flag2;

	// acquire the spinlock for the list
	spin_lock_irqsave(&lock_list, flag2);

	list_for_each_safe(it1, it2, &tracer_list) {
		elem = list_entry(it1, struct tracer_proc, list);

		// update the number of mutex_unlock() calls
		if (pid == elem->pid) {

			// acquire the rwlock to modify the variable
			write_lock_irqsave(&elem->lock_unlock_calls, flag1);
			elem->mutex_unlock_calls += 1;
			write_unlock_irqrestore(&elem->lock_unlock_calls,
						flag1);

			break;
		}
	}

	// release the spinlock for the list
	spin_unlock_irqrestore(&lock_list, flag2);

	jprobe_return();
}

static void __sched tracer_mutex_lock(struct mutex *lock, unsigned int subclass)
{
	struct list_head *it1, *it2;
	struct tracer_proc *elem;
	int pid = current->pid;
	unsigned long flag1, flag2;

	// acquire the spinlock for the list
	spin_lock_irqsave(&lock_list, flag2);

	list_for_each_safe(it1, it2, &tracer_list) {
		elem = list_entry(it1, struct tracer_proc, list);

		// update the number of mutex_lock() calls
		if (pid == elem->pid) {

			// acquire the rwlock to modify the variable
			write_lock_irqsave(&elem->lock_lock_calls, flag1);
			elem->mutex_lock_calls += 1;
			write_unlock_irqrestore(&elem->lock_lock_calls, flag1);

			break;
		}
	}

	// release the spinlock for the list
	spin_unlock_irqrestore(&lock_list, flag2);

	jprobe_return();
}

static void print_value(struct seq_file *m, int value, const char *final_string)
{
	char buff[BUFFER_SIZE];

	memset(buff, 0, BUFFER_SIZE);
	sprintf(buff, "%d", value);
	seq_puts(m, buff);
	seq_puts(m, final_string);
}

static unsigned long sum_array(unsigned long array[MAX_COUNT], int count)
{
	unsigned long sum = 0;
	int i;

	for (i = 0; i < count; i++)
		sum += array[i];

	return sum;
}

static int tracer_proc_show(struct seq_file *m, void *v)
{
	struct list_head *it1, *it2;
	struct tracer_proc *elem;
	int sum_alloc, sum_free, value_kfree, value_kmalloc;
	unsigned long flag1, flag2;

	seq_puts(m, "PID\tkmalloc\tkfree\tkmalloc_mem\tkfree_mem\tsched\tup\tdown\tlock\tunlock\n");

	// acquire the spinlock for the list
	spin_lock_irqsave(&lock_list, flag2);

	list_for_each_safe(it1, it2, &tracer_list) {
		elem = list_entry(it1, struct tracer_proc, list);

		// acquire the rwlock for kmalloc
		read_lock_irqsave(&elem->lock_kmalloc, flag1);
		value_kmalloc = elem->kmalloc_calls;
		sum_alloc = sum_array(elem->alloc, elem->kmalloc_calls);
		read_unlock_irqrestore(&elem->lock_kmalloc, flag1);

		// acquire the rwlock for kfree
		read_lock_irqsave(&elem->lock_kfree, flag1);
		value_kfree = elem->kfree_calls;
		sum_free = sum_array(elem->free, elem->kfree_calls);
		read_unlock_irqrestore(&elem->lock_kfree, flag1);

		print_value(m, elem->pid, "\t");
		print_value(m, value_kmalloc, "\t");
		print_value(m, value_kfree, "\t");
		print_value(m, sum_alloc, "\t");
		print_value(m, sum_free, "\t");

		// acquire the rwlock to print the variable
		read_lock_irqsave(&elem->lock_schedule_calls, flag1);
		print_value(m, elem->schedule_calls, "\t");
		read_unlock_irqrestore(&elem->lock_schedule_calls, flag1);

		// acquire the rwlock to print the variable
		read_lock_irqsave(&elem->lock_up_calls, flag1);
		print_value(m, elem->up_calls, "\t");
		read_unlock_irqrestore(&elem->lock_up_calls, flag1);

		// acquire the rwlock to print the variable
		read_lock_irqsave(&elem->lock_down_calls, flag1);
		print_value(m, elem->down_interruptible_calls, "\t");
		read_unlock_irqrestore(&elem->lock_down_calls, flag1);

		// acquire the rwlock to print the variable
		read_lock_irqsave(&elem->lock_lock_calls, flag1);
		print_value(m, elem->mutex_lock_calls, "\t");
		read_unlock_irqrestore(&elem->lock_lock_calls, flag1);

		// acquire the rwlock to print the variable
		read_lock_irqsave(&elem->lock_unlock_calls, flag1);
		print_value(m, elem->mutex_unlock_calls, "\n");
		read_unlock_irqrestore(&elem->lock_unlock_calls, flag1);
	}

	// release the spinlock for the list
	spin_unlock_irqrestore(&lock_list, flag2);

	return 0;
}

static int kmalloc_entry_handler(struct kretprobe_instance *ri,
				 struct pt_regs *regs)
{
	struct private_data *data = (struct private_data *) ri->data;

	data->size = regs->ax;

	return 0;
}

static int kmalloc_ret_handler(struct kretprobe_instance *ri,
			       struct pt_regs *regs)
{
	struct private_data *data = (struct private_data *) ri->data;
	struct list_head *it1, *it2;
	struct tracer_proc *elem;
	int pid = current->pid;
	unsigned long flag1, flag2;

	// acquire the spinlock for the list
	spin_lock_irqsave(&lock_list, flag2);

	list_for_each_safe(it1, it2, &tracer_list) {
		elem = list_entry(it1, struct tracer_proc, list);

		// this kmalloc() call is from a monitored process
		if (elem->pid == pid) {

			// acquire the rwlock for the kmalloc() call
			write_lock_irqsave(&elem->lock_kmalloc, flag1);

			// save the amount of requested size by kmalloc
			elem->alloc[elem->kmalloc_calls] = data->size;

			// save the address returned by this kmalloc() call
			elem->address[elem->kmalloc_calls] = regs->ax;
			// increase the number of kmalloc() calls
			elem->kmalloc_calls += 1;

			// release the rwlock for the kamlloc() call
			write_unlock_irqrestore(&elem->lock_kmalloc, flag1);

			break;
		}
	}

	// release the spinlock for the list
	spin_unlock_irqrestore(&lock_list, flag2);

	return 0;
}

bool cmp_addr(char objp_buff[BUFFER_SIZE], unsigned long addr)
{
	char addr_buff[BUFFER_SIZE];

	memset(addr_buff, 0, BUFFER_SIZE);
	sprintf(addr_buff, "%08lx", addr);

	return strcmp(addr_buff, objp_buff) == 0;
}

static void tracer_kfree(const void *objp)
{
	struct list_head *it1, *it2;
	struct tracer_proc *elem;
	int i, pid = current->pid;
	char objp_buff[BUFFER_SIZE];
	unsigned long flag1, flag2, flag3;

	if (objp != NULL) {
		memset(objp_buff, 0, BUFFER_SIZE);
		sprintf(objp_buff, "%p", objp);
	} else
		goto exit_jprobe;

	// acquire the spinlock for the list
	spin_lock_irqsave(&lock_list, flag3);

	list_for_each_safe(it1, it2, &tracer_list) {
		elem = list_entry(it1, struct tracer_proc, list);

		if (elem->pid == pid) {

			// acquire the rwlock for the kfree call
			write_lock_irqsave(&elem->lock_kfree, flag1);

			// acquire the rwlock for the kmalloc() call
			read_lock_irqsave(&elem->lock_kmalloc, flag2);

			for (i = elem->kmalloc_calls - 1; i >= 0; i--) {

				if (cmp_addr(objp_buff, elem->address[i])) {

					// update the amount of memory
					// freed by kfree()
					elem->free[elem->kfree_calls] =
						elem->alloc[i];

					// increase the number of kfree() calls
					elem->kfree_calls += 1;

					break;
				}
			}

				// release the rwlock for the kmalloc() call
			read_unlock_irqrestore(&elem->lock_kmalloc, flag2);

			// release the acquired lock for the kfree() call
			write_unlock_irqrestore(&elem->lock_kfree, flag1);

			break;
		}
	}

	// release the spinlock for the list
	spin_unlock_irqrestore(&lock_list, flag3);

exit_jprobe:
	jprobe_return();
}

static int tracer_proc_read_open(struct inode *inode, struct file *file)
{
	return single_open(file, tracer_proc_show, NULL);
}

static int tracer_open(struct inode *inode, struct file *file)
{
	return 0;
}

static int tracer_close(struct inode *inode, struct file *file)
{
	return 0;
}

static int add_elem(pid_t pid)
{
	unsigned long flag1;
	struct tracer_proc *new = NULL;

	new = alloc_tracer_proc_elem(pid);
	if (!new)
		return ERROR_ALLOC;

	// acquire the spinlock for the list
	spin_lock_irqsave(&lock_list, flag1);

	list_add(&new->list, &tracer_list);

	// release the spinlock for the list
	spin_unlock_irqrestore(&lock_list, flag1);

	return 0;
}

static void remove_elem(pid_t pid)
{
	unsigned long flag1;
	struct tracer_proc *elem = NULL;
	struct list_head *it1, *it2;

	// acquire the spinlock for the list
	spin_lock_irqsave(&lock_list, flag1);

	list_for_each_safe(it1, it2, &tracer_list) {
		elem = list_entry(it1, struct tracer_proc, list);

		if (elem->pid == pid) {
			list_del(it1);
			break;
		}
	}

	// release the spinlock for the list
	spin_unlock_irqrestore(&lock_list, flag1);

	if (elem != NULL) {
		kfree(elem);
		elem = NULL;
	}

}

static long tracer_ioctl(struct file *file, unsigned int cmd,
			 unsigned long arg)
{
	int error;

	switch (cmd) {
	case TRACER_ADD_PROCESS:

		error = add_elem((pid_t) arg);
		if (error != 0) {
			pr_err("error add_elem");
			return ERROR_ALLOC;
		}

		break;
	case TRACER_REMOVE_PROCESS:

		remove_elem((pid_t) arg);

		break;
	default:
		return -ENOTTY;
	}
	return 0;
}

static void purge_list(void)
{
	struct list_head *it1, *it2;
	struct tracer_proc *elem;
	unsigned long flag1;

	// acquire the spinlock for the list
	spin_lock_irqsave(&lock_list, flag1);

	list_for_each_safe(it1, it2, &tracer_list) {
		elem = list_entry(it1, struct tracer_proc, list);

		list_del(it1);
		kfree(elem);
		elem = NULL;
	}

	// release the spinlock for the list
	spin_unlock_irqrestore(&lock_list, flag1);
}

static int tracer_init(void)
{
	int error;

	INIT_LIST_HEAD(&tracer_list);

	// initialise spinlock for acces to the list
	spin_lock_init(&lock_list);

	// bind the miscdevice structure with this device
	error = misc_register(&tracer_device);
	if (error) {
		pr_err("Error at misc_register\n");
		return error;
	}

	// create procfs entry
	proc_tracer_read = proc_create(procfs_file_read, 0000, NULL,
				       &procfs_fops);
	if (!proc_tracer_read)
		goto remove_miscdevice;

	// register kretprobe for kmalloc
	kmalloc_kretprobe.kp.symbol_name = "__kmalloc";
	error = register_kretprobe(&kmalloc_kretprobe);
	if (error < 0) {
		pr_err("register_kretprobe failed, returned %d\n", error);
		goto remove_proc;
	}

	// register jprobe for kfree
	kfree_jprobe.kp.symbol_name = "kfree";
	error = register_jprobe(&kfree_jprobe);
	if (error < 0) {
		pr_err("register_jprobe failed for kfree, returned %d\n",
		       error);
		goto remove_kmalloc;
	}

	// register jprobe for mutex_lock
	mutex_lock_jprobe.kp.symbol_name = "mutex_lock_nested";
	error = register_jprobe(&mutex_lock_jprobe);
	if (error < 0) {
		pr_err("register_jprobe failed for mutex_lock, returned %d\n",
		       error);
		goto remove_kfree;
	}

	mutex_unlock_jprobe.kp.symbol_name = "mutex_unlock";
	error = register_jprobe(&mutex_unlock_jprobe);
	if (error < 0) {
		pr_err("register_jprobe failed for unlock, returned %d\n",
		       error);
		goto remove_mutex_lock;
	}

	schedule_jprobe.kp.symbol_name = "schedule";
	error = register_jprobe(&schedule_jprobe);
	if (error < 0) {
		pr_err("register_jprobe failed for schedule, returned %d\n",
		       error);
		goto remove_mutex_unlock;
	}

	up_jprobe.kp.symbol_name = "up";
	error = register_jprobe(&up_jprobe);
	if (error < 0) {
		pr_err("register_jprobe failed for up, returned %d\n", error);
		goto remove_schedule;
	}

	down_jprobe.kp.symbol_name = "down_interruptible";
	error = register_jprobe(&down_jprobe);
	if (error < 0) {
		pr_err("register_jprobe failed for down, returned %d\n",
		       error);
		goto remove_up;
	}

	return 0;

remove_up:
	unregister_jprobe(&up_jprobe);

remove_schedule:
	unregister_jprobe(&schedule_jprobe);

remove_mutex_unlock:
	unregister_jprobe(&mutex_unlock_jprobe);

remove_mutex_lock:
	unregister_jprobe(&mutex_lock_jprobe);

remove_kfree:
	unregister_jprobe(&kfree_jprobe);

remove_kmalloc:
	unregister_kretprobe(&kmalloc_kretprobe);

remove_proc:
	proc_remove(proc_tracer_read);

remove_miscdevice:
	misc_deregister(&tracer_device);

	return -ENOMEM;

}

static void tracer_exit(void)
{
	// clear the content of the list
	purge_list();

	// release the miscdevice structure associated with this device
	misc_deregister(&tracer_device);

	// remove procfs entry
	proc_remove(proc_tracer_read);

	// unregister kretprobe for kmalloc
	unregister_kretprobe(&kmalloc_kretprobe);

	// unregister jprobe for kfree
	unregister_jprobe(&kfree_jprobe);

	// unregister jprobe for mutex_lock
	unregister_jprobe(&mutex_lock_jprobe);

	// unregister jprobe for mutex_unloc
	unregister_jprobe(&mutex_unlock_jprobe);

	// unregister jprobe for schedule
	unregister_jprobe(&schedule_jprobe);

	// unregister jprobe for up
	unregister_jprobe(&up_jprobe);

	// unregister jprobe for down_interruptible
	unregister_jprobe(&down_jprobe);
}

module_init(tracer_init);
module_exit(tracer_exit);

#endif
